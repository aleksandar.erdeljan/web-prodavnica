import flask
from flask.blueprints import Blueprint
from utils.db import mysql
from flask_jwt_extended import get_jwt, jwt_required


pravo_pristupa_bp = Blueprint("pravo_pristupa_bp", __name__)

@pravo_pristupa_bp.route("", methods = ["GET"])
@jwt_required()
def getAllPravaPristupa():
    cursor = mysql.get_db().cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("select * from pravo_pristupa")
        prava = cursor.fetchall()
        return flask.jsonify(prava)
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@pravo_pristupa_bp.route("/<int:id>", methods = ["GET"])
@jwt_required()
def getOnePravoPristupa(id):
    cursor = mysql.get_db().cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("select * from pravo_pristupa where id = %s", (id,))
        pravo = cursor.fetchone()

        if pravo is not None:
            return flask.jsonify(pravo)
        response = {"message": "Not Found pravo_pristupa"}
        return flask.jsonify(response), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@pravo_pristupa_bp.route("", methods=["POST"])
@jwt_required()
def createPravoPristupa():
    db= mysql.get_db()
    cursor= db.cursor()
    body= flask.request.json
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("insert into pravo_pristupa(naziv) values(%(naziv)s)", body)
        db.commit()
        return flask.jsonify(body), 201
    return flask.jsonify({"message": "Not Authorized access for that"}), 403


@pravo_pristupa_bp.route("/<int:id>", methods= ["PUT"])
@jwt_required()
def updatePravoPristupa(id):
    db= mysql.get_db()
    cursor = db.cursor()
    pravo_pristupa= dict(flask.request.json)
    pravo_pristupa["id"]= id
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("update pravo_pristupa set naziv=%(naziv)s where id=%(id)s", pravo_pristupa)
        db.commit()
        if cursor.rowcount>0:
            cursor.execute("select naziv from pravo pristupa where id=%s", (id,))
            updatedPravoPristupa= cursor.fetchone()
            return flask.jsonify(updatedPravoPristupa)
        else:
            return flask.jsonify({"message":"Nothing for update"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403
    

@pravo_pristupa_bp.route("/<int:id>", methods= ["DELETE"])
@jwt_required()
def deletePravoPristupa():
    db = mysql.get_db()
    cursor = db.cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("delete from pravo_pristupa where id=%s", (id,))
        db.commit()
        if cursor.rowcount>0:
            return flask.jsonify({"message":"Deleted succesfully"})
        else:
            return flask.jsonify({"message":"Not found pravo pristupa"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403