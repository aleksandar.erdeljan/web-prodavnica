import flask 
from flask.blueprints import Blueprint
from utils.db import mysql
from flask_jwt_extended import get_jwt, jwt_required

racun_bp = Blueprint("racun_bp", __name__)

@racun_bp.route("", methods = ["GET"])
@jwt_required()
def getAllRacuni():
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from racun")
    racuni = cursor.fetchall()
    return flask.jsonify(racuni)

@racun_bp.route("/<int:id>", methods = ["GET"])
@jwt_required()
def getOneRacun(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from racun where id = %s", (id,))
    racun = cursor.fetchone()

    if racun is not None:
        return flask.jsonify(racun)
    response = {"message":"Not Found Racun"}
    return flask.jsonify(response), 404

@racun_bp.route("/kupac/<int:id>", methods= ["GET"])
@jwt_required()
def getAllRacuniByKupac(id):
    cursor = mysql.get_db().cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("select * from racun where kupac_id= %s", (id,))
        racuni= cursor.fetchall()
        return flask.jsonify(racuni)
    return flask.jsonify({"message":"Not Authorized access for that"}), 403

@racun_bp.route("/kupac/nerealizovan/<int:id>", methods= ["GET"])
@jwt_required()
def getAllNerealizovaniRacuniByKupac(id):
    cursor = mysql.get_db().cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "kupac" in prava_pristupa:
        cursor.execute("select * from racun where kupac_id= %s and status= 'nerealizovan'", (id,))
        racuni= cursor.fetchall()
        return flask.jsonify(racuni)
    return flask.jsonify({"message":"Not Authorized access for that"}), 403

@racun_bp.route("/kupac/realizovan/<int:id>", methods= ["GET"])
@jwt_required()
def getAllRealizovaniRacuniByKupac(id):
    cursor = mysql.get_db().cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "kupac" in prava_pristupa:
        cursor.execute("select * from racun where kupac_id= %s and status= 'realizovan'", (id,))
        racuni= cursor.fetchall()
        return flask.jsonify(racuni)
    return flask.jsonify({"message":"Not Authorized access for that"}), 403

@racun_bp.route("", methods = ["POST"])
@jwt_required()
def createRacun():
    db = mysql.get_db()
    cursor = db.cursor()
    flask.request.json.setdefault("status", "nerealizovan")
    body= flask.request.json
    prava_pristupa = get_jwt().get("prava_pristupa")
    print(prava_pristupa)
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("insert into racun(vreme_izdavanja, status, kupac_id, cena) values(now(), %(status)s, %(kupac_id)s, %(cena)s)", body)
        db.commit()
        cursor.execute("select * from racun order by id desc limit 1")
        racun= cursor.fetchone()
        return flask.jsonify(racun), 201
    return flask.jsonify({"message":"Not Authorized access for that"}), 403


@racun_bp.route("/porudzbina", methods = ["POST"])
@jwt_required()
def createRacunPorudzbina():
    db = mysql.get_db()
    cursor = db.cursor()
    flask.request.json.setdefault("status", "nerealizovan")
    body= flask.request.json
    prava_pristupa = get_jwt().get("prava_pristupa")
    print(prava_pristupa)
    korpa = body['korpa']
    if "kupac" in prava_pristupa:
        cursor.execute(f"insert into racun(vreme_izdavanja, status, kupac_id, cena) values(now(), '{body['status']}','{body['kupac_id']}', '{body['cena']}')")
        db.commit()
        cursor.execute("select * from racun order by id desc limit 1")
        racun= cursor.fetchone()
        for stavka in korpa:
            print(stavka['id'], stavka['kolicina'])
            cursor.execute(f"insert into stavka_racuna(kolicina, racun_id, roba_id) values('{stavka['kolicina']}','{racun['id']}', '{stavka['id']}')")
            db.commit()
        return flask.jsonify(racun), 201
    return flask.jsonify({"message":"Not Authorized access for that"}), 403

@racun_bp.route("/<int:id>", methods= ["PUT"])
@jwt_required()
def updateRacun(id):
    db = mysql.get_db()
    cursor = db.cursor()
    racun= dict(flask.request.json)
    racun["id"]= id
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("update racun set status=%(status)s, kupac_id=%(kupac_id)s, cena=%(cena)s where id=%(id)s ", racun)
        db.commit()
        if cursor.rowcount>0:
            cursor.execute("select vreme_izdavanja, status, kupac_id, cena from racun where id=%s", (id,))
            updatedRacun = cursor.fetchone()
            return flask.jsonify(updatedRacun)
        else:
            return flask.jsonify({"message":"Nothing for update"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403


@racun_bp.route("/<int:id>", methods= ["DELETE"])
@jwt_required()
def deleteRacun(id):
    db = mysql.get_db()
    cursor = db.cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("delete from racun where id=%s", (id,))
        db.commit()
        if cursor.rowcount>0:
            return flask.jsonify({"message":"deleted succesfully"})
        else:
            return flask.jsonify({"message":"Not found in racun"})
    return flask.jsonify({"message": "Not Authorized access for that"}), 403