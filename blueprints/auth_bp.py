import flask
from flask.blueprints import Blueprint
from flask_jwt_extended import get_jwt, create_access_token, jwt_required
from utils.db import mysql

auth_bp = Blueprint("auth_bp", __name__)

@auth_bp.route("/registration", methods = ["POST"])
def registration():
    try:
        db = mysql.get_db()
        cursor = db.cursor()
        body = flask.request.json  
        cursor.execute("select * from korisnik where korisnicko_ime=%s", (body["korisnicko_ime"],))
        korisnik= cursor.fetchone()
        if korisnik is not None:
            return flask.jsonify({"message":"Korisnik already exist"}), 400    
        cursor.execute("insert into korisnik(ime, prezime, korisnicko_ime, lozinka) values(%(ime)s, %(prezime)s, %(korisnicko_ime)s, %(lozinka)s)", body)
        db.commit()
        cursor.execute("insert into dodeljena_prava (korisnik_id, pravo_pristupa_id, vreme_pristupa) values(%s, %s, now())", (cursor.lastrowid, 3,))
        db.commit()
        return flask.jsonify(body), 201
    except Exception as ex:
        return flask.jsonify({"message":"Bad request"}), 400

@auth_bp.route("/login", methods= ["POST"])
def login():
    try:
        cursor = mysql.get_db().cursor()
        body = flask.request.json
        cursor.execute("select * from korisnik where korisnicko_ime= %(korisnicko_ime)s and lozinka= %(lozinka)s", body)
        korisnik = cursor.fetchone()
        if korisnik is not None:
            cursor.execute("SELECT naziv FROM dodeljena_prava dp inner join pravo_pristupa pp on (dp.pravo_pristupa_id=pp.id) where korisnik_id= %s", (korisnik["id"],))
            prava_pristupa = cursor.fetchall()
            prava= []
            for p in prava_pristupa:
                prava.append(p["naziv"])
            access_token = create_access_token(
                identity= korisnik["korisnicko_ime"], 
                additional_claims= {
                    "prava_pristupa": prava, "id": korisnik["id"]
                }
            )
            return flask.jsonify({"token": access_token, "prava_pristupa": prava, "id": korisnik["id"]})
        return flask.jsonify({"message":"Not found korisnik"}), 404
    except Exception as ex:
        return flask.jsonify({"message":"Bad request"}), 400