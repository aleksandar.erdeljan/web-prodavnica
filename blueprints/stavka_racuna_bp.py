import flask 
from flask.blueprints import Blueprint
from utils.db import mysql
from flask_jwt_extended import get_jwt, jwt_required

stavka_racuna_bp = Blueprint("stavka_racuna_bp", __name__)

@stavka_racuna_bp.route("", methods= ["GET"])
@jwt_required
def getAllStavkeRacuna():
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from stavka_racuna")
    stavke = cursor.fetchall()
    return flask.jsonify(stavke)

@stavka_racuna_bp.route("/<int:id>", methods = ["GET"])
@jwt_required()
def getOneStavkaRacuna(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from stavka_racuna where id = %s", (id,))
    stavka = cursor.fetchone()

    if stavka is not None:
        return flask.jsonify(stavka)
    response = {"message":"Not found stavka_racuna"}
    return flask.jsonify(response), 404

@stavka_racuna_bp.route("/racun/<int:id>", methods= ["GET"])
@jwt_required()
def getAllStavkeByRacun(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from stavka_racuna sr left join roba r on (sr.roba_id = r.id) where racun_id= %s", (id,))
    stavke = cursor.fetchall()
    return flask.jsonify(stavke)

@stavka_racuna_bp.route("", methods= ["POST"])
def createStavkaRacuna():
    db= mysql.get_db()
    cursor= db.cursor()
    body= flask.request.json
    prava_pristupa = get_jwt().get(prava_pristupa)
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("insert into stavka_racuna(kolicina, racun_id, roba_id) values(%(kolicina)s, %(racun_id)s, %(roba_id)s)", body)
        db.commit()
        return flask.jsonify(body), 201
    return flask.jsonify({"message": "Not Authorized access for that"}), 403


@stavka_racuna_bp.route("/<int:id>", methods= ["PUT"])
@jwt_required()
def updateStavkaRacuna(id):
    db = mysql.get_db()
    cursor = db.cursor()
    stavka_racuna= dict(flask.request.json)
    stavka_racuna["id"]= id
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("update stavka_racuna set kolicina=%(kolicina)s, racun_id=%(racun_id)s, roba_id=%(roba_id)s where id=%(id)s", stavka_racuna)
        db.commit()
        if cursor.rowcount>0:
            cursor.execute("select kolicina, racun_id, roba_id from stavka_racuna where id=%s", (id,))
            updatedStavkaRacuna= cursor.fetchone()
            return flask.jsonify(updatedStavkaRacuna)
        else:
            return flask.jsonify({"message":"Nothing for update"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@stavka_racuna_bp.route("/<int:id>", methods = ["DELETE"])
def deleteStavkaRacuna(id):
    db = mysql.get_db()
    cursor = db.cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("delete from stavka_racuna where id=%s", (id,))
        db.commit()
        if cursor.rowcount>0:
            return flask.jsonify({"message":"Deleted succesfully"})
        else:
            return flask.jsonify({"message":"Not found in stavka racuna"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403