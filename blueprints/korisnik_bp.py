import flask 
from flask.blueprints import Blueprint
from utils.db import mysql
from flask_jwt_extended import get_jwt, jwt_required

korisnik_bp = Blueprint("korisnik_bp", __name__)

@korisnik_bp.route("", methods= ["GET"])
@jwt_required()
def getAllKorisnici():
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from korisnik")
    korisnici= cursor.fetchall()
    return flask.jsonify(korisnici)

@korisnik_bp.route("/<int:id>", methods= ["GET"])
@jwt_required()
def getOneKorisnik(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from korisnik where id= %s", (id,))
    korisnik = cursor.fetchone()
    
    if korisnik is not None:
        return flask.jsonify(korisnik)
    response= {"message": "Not Found Korisnik"}
    return flask.jsonify(response) , 404

@korisnik_bp.route("", methods = ["POST"])
@jwt_required()
def createKorisnik():
    db = mysql.get_db()
    cursor = db.cursor()
    body = flask.request.json
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("insert into korisnik(ime, prezime, korisnicko_ime, lozinka) values(%(ime)s, %(prezime)s, %(korisnicko_ime)s, %(lozinka)s)", body)
        db.commit()
        return flask.jsonify(body), 201
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@korisnik_bp.route("/<int:id>", methods= ["PUT"])
@jwt_required()
def updateKorisnik(id):
    db = mysql.get_db()
    cursor = db.cursor()
    korisnik = dict(flask.request.json)
    korisnik["id"]= id
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("update korisnik set ime=%(ime)s, prezime=%(prezime)s, korisnicko_ime=%(korisnicko_ime)s, lozinka=%(lozinka)s where id=%(id)s", korisnik)
        db.commit()
        if cursor.rowcount>0:
            cursor.execute("select ime, prezime, korisnicko_ime, lozinka from korisnik where id=%s", (id,))
            updatedKorisnik= cursor.fetchone()
            return flask.jsonify(updatedKorisnik)
        else:
            return flask.jsonify({"message":"Nothing for update"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@korisnik_bp.route("/<int:id>", methods= ["DELETE"])
@jwt_required()
def deleteKorisnik(id):
    db= mysql.get_db()
    cursor = db.cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("delete from korisnik where id=%s", (id,))
        db.commit()
        if cursor.rowcount>0:
            return flask.jsonify({"message":"Deleted succesfully"})
        else:
            return flask.jsonify({"message":"Not found in korisnik"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403