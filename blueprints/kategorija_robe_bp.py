import flask
from flask.blueprints import Blueprint
from utils.db import mysql
from flask_jwt_extended import get_jwt, jwt_required

kategorija_robe_bp = Blueprint("kategorija_robe_bp", __name__)

@kategorija_robe_bp.route("", methods = ["GET"])
@jwt_required()
def getAllKategorijaRobe():
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from kategorija_robe")
    kategorije = cursor.fetchall()
    return flask.jsonify(kategorije)

@kategorija_robe_bp.route("/<int:id>", methods = ["GET"])
@jwt_required()
def getOneKategorijaRobe(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from kategorija_robe where id = %s", (id,))
    kategorija = cursor.fetchone()

    if kategorija is not None:
        return flask.jsonify(kategorija)
    response = {"message": "Not Found Kategorija"}
    return flask.jsonify(response), 404

@kategorija_robe_bp.route("", methods= ["POST"])
@jwt_required()
def createKategorijaRobe():
    db = mysql.get_db()
    cursor = db.cursor()
    body = flask.request.json
    prava_pristupa = get_jwt().get("prava_pristupa")
    print(prava_pristupa)
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("insert into kategorija_robe(naziv, opis) values(%(naziv)s, %(opis)s)", body)
        db.commit()
        return flask.jsonify(body), 201
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@kategorija_robe_bp.route("/<int:id>", methods= ["PUT"])
@jwt_required()
def updateKategorijaRobe(id):
    db= mysql.get_db()
    cursor= db.cursor()
    kategorijaRobe= dict(flask.request.json)
    kategorijaRobe["id"]= id
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("update kategorija_robe set naziv=%(naziv)s, opis=%(opis)s where id= %(id)s", kategorijaRobe)
        db.commit()
        if cursor.rowcount>0:
            cursor.execute("select naziv, opis from kategorija_robe where id=%s", (id,))
            updatedKategorijaRobe= cursor.fetchone()
            return flask.jsonify(updatedKategorijaRobe)
        else:
            return flask.jsonify({"message":"Nothing for update"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403
    
@kategorija_robe_bp.route("/<int:id>", methods= ["DELETE"])
@jwt_required()
def deleteKategorijaRobe(id):
    db= mysql.get_db()
    cursor = db.cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("delete from kategorija_robe where id=%s", (id,))
        db.commit()
        if cursor.rowcount>0:
            return flask.jsonify({"message":"deleted succesfully"})
        else:
            return flask.jsonify({"message":"Not found kategorija robe"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403






