import flask
from flask.blueprints import Blueprint
from utils.db import mysql
from flask_jwt_extended import get_jwt, jwt_required

dodeljena_prava_bp = Blueprint("dodeljena_prava_bp", __name__)

@dodeljena_prava_bp.route("", methods= ["GET"])
@jwt_required()
def getAllDodeljenaPrava():
    cursor = mysql.get_db().cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("select * from dodeljena_prava")
        dodeljena = cursor.fetchall()
        return flask.jsonify(dodeljena)
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@dodeljena_prava_bp.route("/<int:id>", methods= ["GET"])
@jwt_required()
def getOneDodeljenoPravo(id):
    cursor = mysql.get_db().cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("select * from dodeljena_prava where id= %s", (id,))
        dodeljeno = cursor.fetchone()

        if dodeljeno is not None:
            return flask.jsonify(dodeljeno)
        response= {"message":"Not Found dodeljeno_pravo"}
        return flask.jsonify(response), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@dodeljena_prava_bp.route("", methods = ["POST"])
@jwt_required()
def createDodeljenaPrava():
    db=mysql.get_db()
    cursor= db.cursor()
    body= flask.request.json
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("insert into dodeljena_prava(korisnik_id, pravo_pristupa_id, vreme_pristupa) values(%(korisnik_id)s, %(pravo_pristupa_id)s, now())", body)
        db.commit()
        return flask.jsonify(body), 201
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@dodeljena_prava_bp.route("/<int:id>", methods= ["PUT"])
@jwt_required()
def updateDodeljenaPrava(id):
    db = mysql.get_db()
    cursor = db.cursor()
    dodeljena_prava= dict(flask.request.json)
    dodeljena_prava["id"]= id
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("update dodeljena_prava set korisnik_id=%(korisnik_id)s, pravo_pristupa_id=%(pravo_pristupa_id)s, vreme_pristupa=%(vreme_pristupa)s where id=%(id)s", dodeljena_prava)
        db.commit()
        if cursor.rowcount>0:
            cursor.execute("select korisnik_id, pravo_pristupa_id, vreme_pristupa from dodeljena_prava where id=%s", (id,))
            updatedDodeljenaPrava= cursor.fetchone()
            return flask.jsonify(updatedDodeljenaPrava)
        else:
            return flask.jsonify({"message":"Nothing for update"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@dodeljena_prava_bp.route("/<int:id>", methods=["DELETE"])
@jwt_required()
def deleteDodeljenaPrava(id):
    db= mysql.get_db()
    cursor = db.cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "admin" in prava_pristupa:
        cursor.execute("delete from dodeljena_prava where id=%s", (id,))
        db.commit()
        if cursor.rowcount>0:
            return flask.jsonify({"message":"Deleted succesfully"})
        else:
            return flask.jsonify({"message":"Not found in dodeljena prava"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403