import flask
from flask.blueprints import Blueprint
from utils.db import mysql
from flask_jwt_extended import get_jwt, jwt_required

roba_bp = Blueprint("roba_bp", __name__)

@roba_bp.route("", methods= ["GET"])
@jwt_required()
def getAllRobe():
    cursor= mysql.get_db().cursor()
    cursor.execute("select * from roba")
    robe = cursor.fetchall()
    return flask.jsonify(robe)

@roba_bp.route("/<int:id>", methods= ["GET"])
@jwt_required()
def getOneRoba(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from roba where id = %s", (id,))
    roba = cursor.fetchone()

    if roba is not None:
        return flask.jsonify(roba)
    response = {"message": "Not found Roba"}
    return flask.jsonify(response), 404

@roba_bp.route("/kategorija/<int:id>", methods= ["GET"])
@jwt_required()
def getAllRobeByKategorija(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("select * from roba where kategorija_id= %s", (id,))
    robe= cursor.fetchall()
    return flask.jsonify(robe)

@roba_bp.route("", methods = ["POST"])
@jwt_required()
def createRoba():
    db= mysql.get_db()
    cursor= db.cursor()
    flask.request.json.setdefault("slika", None)
    body = flask.request.json
    prava_pristupa= get_jwt().get("prava_pristupa")
    print(prava_pristupa)
    if "rokovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("insert into roba (naziv, slika, opis, cena, stanje, kategorija_id) values(%(naziv)s, %(slika)s, %(opis)s, %(cena)s, %(stanje)s, %(kategorija_id)s)", body)
        db.commit()
        return flask.jsonify(body), 201
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@roba_bp.route("/<int:id>", methods= ["PUT"])
@jwt_required()
def updateRoba(id):
    db= mysql.get_db()
    cursor = db.cursor()
    roba= dict(flask.request.json)
    roba["id"]= id 
    roba.setdefault("slika", None)
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("update roba set naziv=%(naziv)s, slika=%(slika)s, opis=%(opis)s, cena=%(cena)s, stanje=%(stanje)s, kategorija_id=%(kategorija_id)s where id=%(id)s", roba)
        db.commit()
        if cursor.rowcount>0:
            cursor.execute("select naziv, slika, opis, cena, stanje, kategorija_id from roba where id=%s", (id,))
            updatedRoba= cursor.fetchone()
            return flask.jsonify(updatedRoba)
        else: 
            return flask.jsonify({"message":"Nothing for update"}), 404
    return flask.jsonify({"message": "Not Authorized access for that"}), 403

@roba_bp.route("/<int:id>", methods= ["DELETE"])
@jwt_required()
def deleteRoba(id):
    db=mysql.get_db()
    cursor = db.cursor()
    prava_pristupa = get_jwt().get("prava_pristupa")
    if "rukovodilac" in prava_pristupa or "admin" in prava_pristupa:
        cursor.execute("delete from roba where id=%s", (id,))
        db.commit()
        if cursor.rowcount>0:
            return flask.jsonify({"message":"deleted succesfully"})
        else:
            return flask.jsonify({"message":"Not found in roba"})
    return flask.jsonify({"message": "Not Authorized access for that"}), 403