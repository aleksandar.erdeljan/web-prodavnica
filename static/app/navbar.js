const isAuthenticated = localStorage.getItem("token");
const prava_pristupa=localStorage.getItem("prava_pristupa");
console.log(prava_pristupa);

if (isAuthenticated != null){
    document.getElementById("login").style.display="none";
    document.getElementById("register").style.display="none";
}

if (isAuthenticated != null && prava_pristupa.includes("admin")){
    document.getElementById("moji-racuni").style.display="none";
    document.getElementById("ponuda-robe").style.display="none";
    document.getElementById("korpa").style.display="none";
}
else if (isAuthenticated != null && prava_pristupa.includes("kupac")){
    console.log("provera")
    document.getElementById("korisnici").style.display="none";
    document.getElementById("kategorija-robe").style.display="none";
    document.getElementById("roba").style.display="none";
    document.getElementById("racuni").style.display="none";
    document.getElementById("prava-pristupa").style.display="none";
    document.getElementById("dodeljeno-pravo").style.display="none";
}
else {
    document.getElementById("login").style.display="block";
    document.getElementById("register").style.display="block";
    document.getElementById("korisnici").style.display="none";
    document.getElementById("kategorija-robe").style.display="none";
    document.getElementById("roba").style.display="none";
    document.getElementById("racuni").style.display="none";
    document.getElementById("prava-pristupa").style.display="none";
    document.getElementById("dodeljeno-pravo").style.display="none";
    document.getElementById("moji-racuni").style.display="none";
    document.getElementById("korpa").style.display="none";
    document.getElementById("ponuda-robe").style.display="none";
    document.getElementById("logout").style.display="none";

}