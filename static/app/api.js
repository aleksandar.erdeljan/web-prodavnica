const api = axios.create();
api.interceptors.request.use(conf => {
    const token = localStorage.getItem("token");
    if (token){
        conf.headers["Authorization"]= "Bearer "+ token; 
    }
    return conf;
});

export default api;