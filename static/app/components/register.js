export default {
    data(){
        return {
            ime: "", prezime: "", korisnicko_ime: "", lozinka: "", error: null, success: false
        }
    },
    methods: {
        async registration(){
            this.error= null;
            this.success = false;
            try{
                const res = await axios.post("api/auth/registration", {
                    ime: this.ime, 
                    prezime: this.prezime,
                    korisnicko_ime: this.korisnicko_ime,
                    lozinka: this.lozinka

                });

                if (res.status == 201){
                    this.success = true;
                    setTimeout(() => {
                        this.$router.push("/login");
                    }, 1000);
                    
                }
                else{
                    this.error = "Registracija nije uspela pokusajte ponovo.";
                }
            } catch(error){
                this.error = "Registracija nije uspela pokusajte ponovo."
            }
        }
    },
    template: `
    <div class="container mt-5 d-flex flex-column mb-5 pr-5 pl-5">
            <div class="container text-center">Registracija</div>
            <form @submit.prevent="registration">
                <div class="form-group">
                    <label for="ime" class="form-label">Ime</label>
                    <input type="text" class="form-control" id="ime" v-model="ime" placeholder="Ime" required>
                </div>
                <div class="form-group">
                    <label for="prezime" class="form-label">Prezime</label>
                    <input type="text" class="form-control" id="prezime" v-model="prezime" placeholder="Prezime" required>
                </div>
                <div class="form-group">
                    <label for="korisnicko_ime" class="form-label">Korisnicko ime</label>
                    <input type="text" class="form-control" id="korisnicko_ime" v-model="korisnicko_ime" placeholder="Korisnicko ime" required>
                </div>
                <div class="form-group">
                    <label for="lozinka" class="form-label">Lozinka</label>
                    <input type="password" class="form-control" id="lozinka" v-model="lozinka" placeholder="Lozinka" required>
                </div>
                <div class="alert alert-danger" v-if="error">{{error}}</div>
                <div class="alert alert-success" v-if="success">Uspesno ste se registrovali!</div>
                <div class="container text-center">
                    <button type="submit" class="btn btn-primary">
                        Registruj se
                    </button>
                </div>
                
            </form>
        </div>
    `
}