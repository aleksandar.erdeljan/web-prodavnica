import api from "../api.js";
export default{
    data(){
        return {
            korpa: [],
            ukupna_cena: 0,
            selected_item: {
                id: null,
                naziv: null,
                slika: null,
                opis: null,
                cena: null,
                stanje: null,
                kolicina: null,
                kategorija_id: null
            }
        }
    },
    mounted(){
       this.getAllRobaFromKorpa();
    },
    methods:{
        getAllRobaFromKorpa(){
            if(localStorage.getItem("korpa") == null){
                this.korpa = [];
            }else{
                this.korpa = JSON.parse(localStorage.getItem("korpa"))
                console.log(this.korpa);
                this.ukupna_cena = this.korpa.reduce((prev, current)=> prev + (current.cena * current.kolicina), 0);
                console.log(this.ukupna_cena);
            }
        },
        deleteRobaFromKorpa(index){
            this.korpa = this.korpa.filter((item, i)=> i != index);
            console.log(this.korpa);
            this.ukupna_cena = this.korpa.reduce((prev, current)=> prev + (current.cena * current.kolicina), 0);
            localStorage.setItem("korpa",JSON.stringify(this.korpa));
        },
        poruciRobu(){
            console.log("porucivanje robe");
            console.log(this.korpa);
            console.log(localStorage.getItem("korisnik_id"));
            api.post("/api/racun/porudzbina", {
                kupac_id: localStorage.getItem("korisnik_id"),
                cena: this.ukupna_cena,
                korpa: this.korpa
                
            }).then( res => {
                console.log("Uspesna porudzbina");
                this.korpa = [];
                localStorage.removeItem("korpa");
                this.ukupna_cena = 0;
                this.showSuccessNotification();
            }).catch(error => {
                console.log(error);
                this.showErrorNotification();
            });
        },
        showSuccessNotification(){
            var toastElList = document.querySelectorAll('#toast1')[0];
            toastElList.classList.add("show");
            setTimeout(() => {
                toastElList.classList.remove("show");
                this.$router.push('/moji_racuni');
            }, 500);
        },
        showErrorNotification(){
            var toastElList = document.querySelectorAll('#toast2')[0];
            toastElList.classList.add("show");
            setTimeout(() => {
                toastElList.classList.remove("show");
            }, 1500);
        }
    },
    template: `
        <div style="min-height: 410px">
        <div class="toast fade hide bg-success text-white p-4 border rounded" id="toast1" style="position: fixed; top: 19%; left: 50%; transform: translate(-50%, -50%); width: 400px; z-index: 1;">
            <div class="toast-header text-white text-center mb-3 border-bottom">
                <span><i class="fa-solid fa-circle-check fa-lg icon-success"></i></span>
                <strong class="me-auto"><i class="bi-gift-fill"></i> Korpa info</strong>
            </div>
            <div class="toast-body text-center">
                <strong class="me-auto">Uspesno ste izvrsili porudzbinu!</strong>
            </div>
        </div>
        <div class="toast fade hide bg-danger text-white p-4 border rounded" id="toast2" style="position: fixed; top: 19%; left: 50%; transform: translate(-50%, -50%); width: 400px; z-index: 1;">
            <div class="toast-header text-white text-center mb-3 border-bottom">
                <span><i class="fa-solid fa-circle-xmark fa-lg icon-success"></i></span>
                <strong class="me-auto"><i class="bi-gift-fill"></i> Korpa info</strong>
            </div>
            <div class="toast-body text-center">
                <strong class="me-auto">Porudzbina nije uspesno izvrsena!</strong>
            </div>
        </div>
            <div class="container">
                <h2 class="text-center mt-4 mb-4">Roba koju ste izabrali</h2>
                <table class="table mb-4 table-hover table-bordered text-center">
                    <thead class="thead-dark">
                        <tr>
                            <th>Naziv</th>
                            <th>Slika</th>
                            <th>Opis</th>
                            <th>Cena</th>
                            <th>Kolicina</th>
                            <th>Akcije</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(roba, index) in korpa" :key="roba.id">
                            <td class="align-middle">{{roba.naziv}}</td>
                            <td class="align-middle">
                                <img v-if="roba.slika" v-bind:src="'/assets/'+roba.slika" alt="Roba" class="img-fluid" style="width: 90px; height: 90px">
                            </td>
                            <td class="align-middle">{{roba.opis}}</td>
                            <td class="align-middle">{{roba.cena}}</td>
                            <td class="align-middle">{{roba.kolicina}}</td>
                            <td class="align-middle">
                                <button type="button" class="btn btn-danger btn-sm" @click="selected_item={...roba}; deleteRobaFromKorpa(index)">Delete</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    <h4>Ukupna cena: {{ukupna_cena}} rsd</h4>
                </div>
                <div class="d-flex p-3 justify-content-end">
                    <button type="button" class="btn btn-warning w-25" :disabled="ukupna_cena == 0" @click="poruciRobu()">
                        Poruci robu
                    </button>
                </div>
            </div>
        </div>
    `
}