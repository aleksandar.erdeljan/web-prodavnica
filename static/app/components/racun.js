import api from "../api.js";
export default{
    data(){
        return {
            racuni: [],
            korisnici: [],
            addshow: false, 
            editshow: false,
            deleteshow: false,
            selected_item: {
                id: null,
                vreme_izdavanja: null,
                status: null,
                kupac_id: null,
                cena: null
            }
        }
    },
    mounted(){
        this.getAllRacuni();
        this.getAllKorisnici();
    },
    methods: {
        getAllRacuni(){
            api.get("/api/racun").then(res => {
                this.racuni = res.data;
            }).catch(error => {
                console.log(error);
            });
        },

        getAllKorisnici(){
            api.get("/api/korisnik").then(res => {
                this.korisnici = res.data;
            }).catch(error => {
                console.log(error);
            });
        },
        createRacun(){
            console.log("nesto");
            console.log(this.selected_item.status);
            console.log(this.selected_item.kupac_id);
            api.post("/api/racun", {
                status: this.selected_item.status,
                kupac_id: this.selected_item.kupac_id,
                cena: this.selected_item.cena
                
            }).then( res => {
                this.getAllRacuni();
                this.addshow = false;
                this.selected_item = {
                    id: null,
                    status: null,
                    kupac_id: null,
                    cena: null
                };

            }).catch(error => {
                console.log(error);
            });
        },
        
        updateRacun(){
            api.put("/api/racun/"+ this.selected_item.id,{
                status: this.selected_item.status,
                kupac_id: this.selected_item.kupac_id,
                cena: this.selected_item.cena
            })
                .then(res =>{
                    this.getAllRacuni();
                    this.editshow = false;
                    this.selected_item = {
                        id: null,
                        vreme_izdavanja: null,
                        status: null,
                        kupac_id: null,
                        cena: null
                    }
                }).catch(error =>{
                    console.log(error);
                });
        },
        clearSelected_item(){
            this.selected_item= {
                id: null,
                vreme_izdavanja: null,
                status: null,
                kupac_id: null,
                cena: null
            }
        },
        deleteRacun(){
            api.delete("/api/racun/"+ this.selected_item.id)
                .then(res =>{
                    this.getAllRacuni();
                    this.clearSelected_item();
                    this.deleteshow = false;
                }).catch(error =>{
                    console.log(error);
                })
        }
    },
    template:`
    <div style="min-height: 410px">
        <div class="container">
            <h2 class="text-center mt-4 mb-4">Tabela racun</h2>
            
            <div class="text-center mb-4">
                <button class="btn btn-success" @click="clearSelected_item(); addshow = true">Add</button>
            </div>

            <table class="table mb-4 table-hover table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Vreme izdavanja</th>
                        <th>Status</th>
                        <th>Kupac</th>
                        <th>Cena</th>
                        <th>Akcije</th>
        
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="racun in racuni" :key="racun.id">
                        <td>{{racun.id}}</td>
                        <td>{{racun.vreme_izdavanja}}</td>
                        <td>{{racun.status}}</td>
                        <td>{{korisnici.find(k => k.id == racun.kupac_id)?.korisnicko_ime}}</td>
                        <td>{{racun.cena}}</td>
                        <td>
                        <button class="btn btn-warning btn-sm" @click="selected_item={...racun}; editshow= true">Edit</button>
                        <button class="btn btn-danger btn-sm" @click="selected_item={...racun}; deleteshow= true">Delete</button>
                        
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': addshow}">
            <div class="modal-dialog" role="ducument">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Add racun</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="addshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="createRacun">
                            <div class="form-group">
                                <label for="racun" class="form-label">Status:</label>
                                <select class="form-control" id="status" :disabled="deleteshow" v-model="selected_item.status" required>
                                    <option value="" disabled selected>Izaberite status</option>
                                    <option value="nerealizovan">nerealizovan</option>
                                    <option value="realizovan">realizovan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="korisnik" class="form-label">Kupac</label>
                                <select class="form-control" id="korisnik" :disabled="deleteshow" v-model="selected_item.kupac_id" required>
                                    <option value="" disabled selected>Izaberite korisnika</option>
                                    <option v-for="k in korisnici" :value="k.id">{{k.korisnicko_ime}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="racun" class="form-label">Cena:</label>
                                <input type="number" min="0" class="form-control" id="cena" v-model="selected_item.cena" placeholder="Cena" required>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="addshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>


        <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': editshow}">
            <div class="modal-dialog" role="ducument">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Edit racun</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="editshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="updateRacun">
                            <div class="form-group">
                                <label for="racun" class="form-label">Status:</label>
                                <select class="form-control" id="status" :disabled="deleteshow" v-model="selected_item.status" required>
                                    <option value="" disabled selected>Izaberite status</option>
                                    <option value="nerealizovan">nerealizovan</option>
                                    <option value="realizovan">realizovan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="korisnik" class="form-label">Kupac</label>
                                <select class="form-control" id="korisnik" :disabled="deleteshow" v-model="selected_item.kupac_id" required>
                                    <option value="" disabled selected>Izaberite korisnika</option>
                                    <option v-for="k in korisnici" :value="k.id">{{k.korisnicko_ime}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="racun" class="form-label">Cena:</label>
                                <input type="number" min="0" class="form-control" id="cena" v-model="selected_item.cena" placeholder="Cena" required>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="editshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" :class="{'d-block': deleteshow}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Delete Racun</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="deleteshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2>Are you sure you want to delete this?</h2>
                        <h4 class="text-danger">Deletion can cause an additional deletions</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" @click="deleteRacun()">YES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" @click="deleteshow=false;">NO</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}