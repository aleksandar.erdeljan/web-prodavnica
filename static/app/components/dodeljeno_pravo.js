import api from "../api.js";
export default {
    data(){
        return {
            dodeljena_prava: [], 
            korisnici: [],
            prava_pristupa: [],
            addshow: false, 
            editshow: false,
            deleteshow: false,
            selected_item: {
                id: null,
                korisnik_id: null,
                pravo_pristupa_id: null,
                vreme_pristupa:null
            },
            korisnik_id: localStorage.getItem("korisnik_id")

        }
    },
    mounted(){
        this.getAllDodeljenaPrava();
        this.getAllKorisnici();
        this.getAllPravaPristupa();
    },
    methods: {
        getAllDodeljenaPrava(){
            api.get("/api/dodeljena_prava").then(res => {
                this.dodeljena_prava = res.data;
            }).catch(error => {
                console.log(error);
            });
        }, 
        getAllKorisnici(){
            api.get("/api/korisnik").then(res => {
                this.korisnici = res.data;
            }).catch(error => {
                console.log(error);
            });
        },
        getAllPravaPristupa(){
            api.get("/api/pravo_pristupa").then(res => {
                this.prava_pristupa = res.data;
            }).catch(error => {
                console.log(error);
            });
        },
        createDodeljenaPrava(){
            console.log(this.selected_item);
            api.post("/api/dodeljena_prava", this.selected_item).then(res => {
                this.getAllDodeljenaPrava();
                this.addshow = false;
                this.selected_item = {
                    id: null,
                    korisnik_id: null,
                    pravo_pristupa_id: null,
                    vreme_pristupa: null 
                };
    
            }).catch(error => {
                console.log(error);
            });
        },
        clearSelected_item(){
            this.selected_item= {
                id: null,
                korisnik_id: null,
                pravo_pristupa_id: null,
                vreme_pristupa: null,
            }
        },
        deleteDodeljenoPravo(){
            api.delete("/api/dodeljena_prava/"+ this.selected_item.id)
                .then(res =>{
                    this.getAllDodeljenaPrava();
                    this.clearSelected_item();
                    this.deleteshow = false;
                }).catch(error =>{
                    console.log(error);
                })
        }
        


    },
    template:`
    <div style="min-height: 410px">
        <div class="container">
            <h2 class="text-center mt-4 mb-4">Tabela dodeljena-prava</h2>
            
            <div class="text-center mb-4">
                <button class="btn btn-success" @click="clearSelected_item(); addshow = true">Add</button>
            </div>

            <table class="table mb-4 table-hover table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Korisnik </th>
                        <th>Pravo pristupa </th>
                        <th>Vreme pristupa</th>
                        <th>Akcije</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="dodeljeno_pravo in dodeljena_prava" :key="dodeljeno_pravo.id">
                        <td>{{dodeljeno_pravo.id}}</td>
                        <td>{{korisnici.find(k => k.id == dodeljeno_pravo.korisnik_id)?.korisnicko_ime}}</td>
                        <td>{{prava_pristupa.find(pp => pp.id == dodeljeno_pravo.pravo_pristupa_id)?.naziv}}</td>
                        <td>{{dodeljeno_pravo.vreme_pristupa}}</td>
                        <td v-if="dodeljeno_pravo.korisnik_id != korisnik_id">
                        <button class="btn btn-danger btn-sm" @click="selected_item={...dodeljeno_pravo}; deleteshow= true">Delete</button>
                        
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': addshow}">
            <div class="modal-dialog" role="ducument">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Add dodeljena-prava</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="addshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="createDodeljenaPrava">
                            <div class="form-group">
                                <label for="korisnik" class="form-label">Korisnik</label>
                                <select class="form-control" id="korisnik" :disabled="deleteshow" v-model="selected_item.korisnik_id" required>
                                    <option value="" disabled selected>Izaberite korisnika</option>
                                    <option v-for="k in korisnici" :value="k.id">{{k.korisnicko_ime}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pravo_pristupa" class="form-label">Pravo pristupa</label>
                                <select class="form-control" id="pravo_pristupa" :disabled="deleteshow" v-model="selected_item.pravo_pristupa_id" required>
                                    <option value="" disabled selected>Izaberite pravo pristupa</option>
                                    <option v-for="pp in prava_pristupa" :value="pp.id">{{pp.naziv}}</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="addshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" :class="{'d-block': deleteshow}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Delete dodeljeno pravo</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="deleteshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2>Are you sure you want to delete this?</h2>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" @click="deleteDodeljenoPravo()">YES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" @click="deleteshow=false;">NO</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}