import api from "../api.js";
export default {
    data(){
        return {
            nerealizovaniRacuni: [],
            realizovaniRacuni: [],
            selected_item: {
                id: null,
                vreme_izdavanja: null,
                status: null,
                cena: null
            },
            stavkeRacunaShow: false,
            stavkeRacuna: []
        }
    },
    mounted(){
        this.getAllNerealizovaniRacuni();
        this.getAllRealizovaniRacuni();
    },
    methods:{
        getAllNerealizovaniRacuni(){
            api.get(`/api/racun/kupac/nerealizovan/${localStorage.getItem('korisnik_id')}`)
                .then(res => {
                    this.nerealizovaniRacuni = res.data;
                })
                .catch(error => {
                    console.log(error);
                });
        },

        getAllRealizovaniRacuni(){
            api.get(`/api/racun/kupac/realizovan/${localStorage.getItem('korisnik_id')}`)
                .then(res => {
                    this.realizovaniRacuni = res.data;
                })
                .catch(error => {
                    console.log(error);
                });
        },

        clearSelectedItem(){
            this.selected_item= {
                id: null,
                vreme_izdavanja: null,
                status: null,
                cena: null
            }
        },
        getStavkeRacuna(){
            api.get(`/api/stavka_racuna/racun/${this.selected_item.id}`)
                .then(res => {
                    this.stavkeRacuna = res.data;
                    console.log(this.stavkeRacuna);
                })
                .catch(error => {
                    console.log(error);
                });
            this.stavkeRacunaShow = true;
        }
    },
    template: `
        <div style="min-height: 410px">
            <div class="container">
                <h2 class="text-center mt-4 mb-4">Vasi nerealizovani racuni</h2>
                <table class="table mb-4 table-hover table-bordered text-center">
                    <thead class="thead-dark">
                        <tr>
                            <th>Sifra Racuna</th>
                            <th>Vreme izdavanja</th>
                            <th>Status</th>
                            <th>Cena</th>
                            <th>Akcije</th>
            
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="racun in nerealizovaniRacuni" :key="racun.id">
                            <td>{{racun.id}}</td>
                            <td>{{racun.vreme_izdavanja}}</td>
                            <td>{{racun.status}}</td>
                            <td>{{racun.cena}}</td>
                            <td>
                            <button class="btn btn-warning btn-sm" @click="selected_item={...racun}; getStavkeRacuna()">Stavke</button>
                            
                            </td>
                        </tr>
                    </tbody>
                </table>

                <h2 class="text-center mt-4 mb-4">Vasi realizovani racuni</h2>
                <table class="table mb-4 table-hover table-bordered text-center">
                    <thead class="thead-dark">
                        <tr>
                            <th>Sifra Racuna</th>
                            <th>Vreme izdavanja</th>
                            <th>Status</th>
                            <th>Cena</th>
                            <th>Akcije</th>
            
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="racun in realizovaniRacuni" :key="racun.id">
                            <td>{{racun.id}}</td>
                            <td>{{racun.vreme_izdavanja}}</td>
                            <td>{{racun.status}}</td>
                            <td>{{racun.cena}}</td>
                            <td>
                            <button class="btn btn-warning btn-sm" @click="selected_item={...racun}; getStavkeRacuna()">Stavke</button>
                            
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': stavkeRacunaShow}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Stavke izabranog racuna sa sifrom: {{selected_item.id}}</h5>
                        </div>
                        <div class="modal-body">
                        <table class="table mb-4 table-hover table-bordered text-center">
                        <thead class="thead-dark">
                            <tr>
                                <th>Naziv</th>
                                <th>Slika</th>
                                <th>Opis</th>
                                <th>Kolicina</th>
                                <th>Cena</th>
                            
                
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="stavka in stavkeRacuna" :key="stavka.id">
                                <td>{{stavka.naziv}}</td>
                                <td>
                                    <img v-if="stavka.slika" v-bind:src="'/assets/'+stavka.slika" alt="Stavka" class="img-fluid" style="width: 90px; height: 90px">
                                </td>
                                <td>{{stavka.opis}}</td>
                                <td>{{stavka.kolicina}}</td>
                                <td>{{stavka.cena}}</td>
                            </tr>
                        </tbody>
                    </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" @click="clearSelectedItem(); stavkeRacunaShow=false">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    `
}