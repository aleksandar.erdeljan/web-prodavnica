export default {
    data(){
        return {
            korisnicko_ime: "", lozinka: "", error: null, success: false
        }
    },
    methods: {
        async login(){
            this.error = null;
            this.success = false;
            try{
                const res = await axios.post("api/auth/login", {
                    korisnicko_ime: this.korisnicko_ime,
                    lozinka: this.lozinka
                });
                const data = res.data;
                if(res.status == 200){
                    console.log(data);
                    //localStorage.clear();
                    localStorage.setItem("korisnik_id", data.id);
                    localStorage.setItem("token", data.token);
                    localStorage.setItem("prava_pristupa", JSON.stringify(data.prava_pristupa));
                    this.success = true;
                    this.$router.push("/");
                    location.reload();
                    
                }else{
                    this.error = "Prijava nije uspela, proverite korisnicko ime i lozinku i pokusajte ponovo";
                }
            }catch(error){
                this.error = "Prijava nije uspela pokusajte ponovo.";
            }
        }
    },
    template: `
    <div class="container mt-5 d-flex flex-column mb-5 pr-5 pl-5" style="min-height: 330px">
            <div class="container text-center">Prijava</div>
            <form @submit.prevent="login">
                <div class="form-group">
                    <label for="korisnicko_ime" class="form-label">Korisnicko ime</label>
                    <input type="text" class="form-control" id="korisnicko_ime" v-model="korisnicko_ime" placeholder="Korisnicko ime" required>
                </div>
                <div class="form-group">
                    <label for="lozinka" class="form-label">Lozinka</label>
                    <input type="password" class="form-control" id="lozinka" v-model="lozinka" placeholder="Lozinka" required>
                </div>
                <div class="alert alert-danger" v-if="error">{{error}}</div>
                <div class="alert alert-success" v-if="success">Uspesno ste se prijavili!</div>
                <div class="container text-center">
                    <button type="submit" class="btn btn-primary">
                        Prijavi se
                    </button>
                </div>
                
            </form>
        </div>

    `
}