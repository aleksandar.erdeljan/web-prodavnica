import api from "../api.js";
export default {
    data(){
        return {
            robe: [],
            kategorije_robe: [],
            addshow: false, 
            editshow: false,
            deleteshow: false,
            selected_item: {
                id: null,
                naziv: null,
                slika: null,
                opis: null,
                cena: null,
                stanje: null,
                kategorija_id: null
            }
        }
    },
    mounted(){
        this.getAllRobe();
        this.getAllKategorijeRobe();
    },
    methods: {
        getAllRobe(){
            api.get("/api/roba").then(res => {
                this.robe = res.data;
            }).catch(error => {
                console.log(error);
            });
        },
        getAllKategorijeRobe(){
            api.get("/api/kategorija_robe").then(res => {
                this.kategorije_robe = res.data;
                console.log("provera")
                console.log(this.kategorije_robe);
            }).catch(error => {
                console.log(error);
            });
        },
        createRoba(){
            api.post("/api/roba", {
                naziv: this.selected_item.naziv, 
                slika: this.selected_item.slika,
                opis: this.selected_item.opis,
                cena: this.selected_item.cena,
                stanje: this.selected_item.stanje,
                kategorija_id: this.selected_item.kategorija_id
            }).then(res => {
                this.getAllRobe();
                this.addshow = false;
                this.selected_item = {
                    id: null,
                    naziv: null,
                    slika: null,
                    opis: null,
                    cena: null,
                    stanje: null,
                    kategorija_id: null
                };

            }).catch(error => {
                console.log(error);
            });
            
        },
        updateRoba(){
            api.put("/api/roba/"+ this.selected_item.id, {
                naziv: this.selected_item.naziv, 
                slika: this.selected_item.slika,
                opis: this.selected_item.opis,
                cena: this.selected_item.cena,
                stanje: this.selected_item.stanje,
                kategorija_id: this.selected_item.kategorija_id
            }).then(res=>{
                    this.editshow = false;
                    this.getAllRobe();
                    this.selected_item = {
                        id: null,
                        naziv: null,
                        slika: null,
                        opis: null,
                        cena: null,
                        stanje: null,
                        kategorija_id: null
                    };
                }).catch(error =>{
                    console.log(error);
                })
        },
        uploadFile(event){
            console.log(event.target.files[0].name);
            this.selected_item.slika= event.target.files[0].name;
        },
        clearSelected_item(){
            this.selected_item= {
                id: null,
                naziv: null,
                slika: null,
                opis: null,
                cena: null,
                stanje: null,
                kategorija_id: null
            }
        },
        deleteRoba(){
            api.delete("/api/roba/"+ this.selected_item.id)
                .then(res =>{
                    this.getAllRobe();
                    this.clearSelected_item();
                    this.deleteshow = false;
                }).catch(error =>{
                    console.log(error);
                })
        }
    },
    template:`
    <div style="min-height: 410px">
        <div class="container">
            <h2 class="text-center mt-4 mb-4">Tabela roba</h2>
            <div class="text-center mb-4">
                <button class="btn btn-success" @click="clearSelected_item(); addshow = true">Add</button>
            </div>
            <table class="table mb-4 table-hover table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Naziv</th>
                        <th>Slika</th>
                        <th>Opis</th>
                        <th>Cena</th>
                        <th>Stanje</th>
                        <th>Kategorija</th>
                        <th>Akcije</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="roba in robe" :key="roba.id">
                        <td>{{roba.id}}</td>
                        <td>{{roba.naziv}}</td>
                        <td>
                            <img v-if="roba.slika" v-bind:src="'/assets/'+roba.slika" alt="Roba" class="img-fluid" style="width: 90px; height: 90px">
                        </td>
                        <td>{{roba.opis}}</td>
                        <td>{{roba.cena}}</td>
                        <td>{{roba.stanje}}</td>
                        <td>{{kategorije_robe.find(kr => kr.id == roba.kategorija_id)?.naziv}}</td>
                        <td>
                        <button class="btn btn-warning btn-sm" @click="selected_item={...roba}; editshow = true">Edit</button>
                        <button class="btn btn-danger btn-sm" @click="selected_item={...roba}; deleteshow= true">Delete</button>
                        
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal" style="overflow-y: auto" role="dialog" tabindex="-1" :class="{'d-block': addshow}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-tittle">Add roba</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="addshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="createRoba">
                            <div class="form-group">
                                <label for="naziv" class="form-label">Naziv:</label>
                                <input type="text" class="form-control" id="naziv" v-model="selected_item.naziv" placeholder="Naziv" required>
                            </div>
                            <div class="form-group">
                                <label for="slika" class="form-label">Slika:</label>
                                <input type="file" class="form-control" id="slika" @change="uploadFile" placeholder="Slika" multiple>
                            </div>
                            <div class="form-group">
                                <label for="opis" class="form-label">Opis:</label>
                                <input type="text" class="form-control" id="opis" v-model="selected_item.opis" placeholder="Opis" required>
                            </div>
                            <div class="form-group">
                                <label for="cena" class="form-label">Cena:</label>
                                <input type="text" class="form-control" id="cena" v-model="selected_item.cena" placeholder="Cena" required>
                            </div>
                            <div class="form-group">
                                <label for="stanje" class="form-label">Stanje:</label>
                                <select class="form-control" id="stanje" :disabled="deleteshow" v-model="selected_item.stanje" required>
                                <option value="" disabled selected>Izaberite stanje</option>
                                <option>dostupno</option>
                                <option>nedostupno</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="kategorija_robe" class="form-label">Kategorija</label>
                                <select class="form-control" id="kategorija_robe" :disabled="deleteshow" v-model="selected_item.kategorija_id" required>
                                    <option value="" disabled selected>Izaberite kategoriju</option>
                                    <option v-for="kk in kategorije_robe" :value="kk.id">{{kk.naziv}}</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="addshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>




        <div class="modal" style="overflow-y: auto" role="dialog" tabindex="-1" :class="{'d-block': editshow}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-tittle">Edit roba</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="editshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="updateRoba">
                            <div class="form-group">
                                <label for="naziv" class="form-label">Naziv:</label>
                                <input type="text" class="form-control" id="naziv" v-model="selected_item.naziv" placeholder="Naziv" required>
                            </div>
                            <div class="form-group">
                                <label for="slika" class="form-label">Slika:</label>
                                <input type="file" class="form-control" id="slika" @change="uploadFile" placeholder="Slika" multiple>
                            </div>
                            <div class="form-group">
                                <label for="opis" class="form-label">Opis:</label>
                                <input type="text" class="form-control" id="opis" v-model="selected_item.opis" placeholder="Opis" required>
                            </div>
                            <div class="form-group">
                                <label for="cena" class="form-label">Cena:</label>
                                <input type="text" class="form-control" id="cena" v-model="selected_item.cena" placeholder="Cena" required>
                            </div>
                            <div class="form-group">
                                <label for="stanje" class="form-label">Stanje:</label>
                                <select class="form-control" id="stanje" :disabled="deleteshow" v-model="selected_item.stanje" required>
                                <option value="" disabled selected>Izaberite stanje</option>
                                <option>dostupno</option>
                                <option>nedostupno</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="kategorija_robe" class="form-label">Kategorija</label>
                                <select class="form-control" id="kategorija_robe" :disabled="deleteshow" v-model="selected_item.kategorija_id" required>
                                    <option value="" disabled selected>Izaberite kategoriju</option>
                                    <option v-for="kk in kategorije_robe" :value="kk.id">{{kk.naziv}}</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="editshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" :class="{'d-block': deleteshow}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Delete Roba</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="deleteshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2>Are you sure you want to delete this?</h2>
                        <h4 class="text-danger">Deletion can cause an additional deletions</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" @click="deleteRoba()">YES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" @click="deleteshow=false;">NO</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}