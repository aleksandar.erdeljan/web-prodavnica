export default{
    created(){
        localStorage.clear();
        this.$router.push("/login");
        location.reload();
    },
    template: `
    <div>Logout</div>
    `

}