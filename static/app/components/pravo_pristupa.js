import api from "../api.js";
export default {
    data(){
        return {
            prava_pristupa: []
        }
    },
    mounted(){
        this.getAllPravaPristupa();
    },
    methods: {
        getAllPravaPristupa(){
            api.get("/api/pravo_pristupa").then(res => {
                this.prava_pristupa = res.data;
            }).catch(error => {
                console.log(error);
            });
        }
    },
    template:`
    <div style="min-height: 410px">
        <div class="container">
            <h2 class="text-center mt-4 mb-4">Tabela prava-pristupa</h2>
            <table class="table mb-4 table-hover table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Naziv</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="pravo_pristupa in prava_pristupa" :key="pravo_pristupa.id">
                        <td>{{pravo_pristupa.id}}</td>
                        <td>{{pravo_pristupa.naziv}}</td>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    `
}