export default{
    template: `
    <div class="container mt-5 d-flex flex-column mb-5" >
            <div class="row">
                <div class="col-md-6">
                    <h1>Dobrodošli u Web Prodavnicu</h1>
                    <p>Pogledajte siroku ponudu nasih proizvoda</p>
                    <p>Istražite naš izbor elektronike, odeće, aksesoara i još mnogo toga.</p>
                    <p>Kupujte sa poverenjem i uživajte u pogodnostima kupovine na mreži.</p>
                </div>
                <div class="col-md-6">
                    <img src="https://picsum.photos/id/1/400/500" alt="Web Prodavnica Slika" class="img-fluid">
                </div>
            </div>
            <div class="bg-light mt-4">
                <div class="container mt-4 mb-5">
                    <h2>O projektu:</h2>
                    <p>Ovaj projekat predstavlja web prodavnicu</p>
                    <p>Predstavlja demonstraciju kako napraviti web sajt, koristeci tehnologije kao sto su flask, vue js my sql i mnoge druge</p>
                    <p>Uzivajte u koriscenju</p>
                </div>
            </div>
        </div>
    
        
    `
}