import api from "../api.js";
export default{
    data(){
        return{
            kategorija_robe: [],
            addshow: false, 
            editshow: false,
            deleteshow: false,
            selected_item: {
                id: null,
                naziv: null,
                opis: null
            }
        }
    },
    mounted(){
        this.getAllKategorijaRobe()
    },
    methods: {
        getAllKategorijaRobe(){
            api.get("/api/kategorija_robe").then(res => {
                this.kategorija_robe = res.data;
            }).catch(error => {
                console.log(error);
            });
        },
        createKategorijaRobe(){
            api.post("/api/kategorija_robe", {
                naziv: this.selected_item.naziv,
                opis: this.selected_item.opis
            }).then(res => {
                this.getAllKategorijaRobe();
                this.addshow = false;
                this.selected_item = {
                    id: null,
                    naziv: null,
                    opis: null
                };
            }).catch(error => {
                console.log(error);
            });
        },
        updateKategorijaRobe(){
            api.put("/api/kategorija_robe/"+ this.selected_item.id, this.selected_item)
                .then(res =>{
                    this.getAllKategorijaRobe();
                    this.editshow = false;
                    this.selected_item = {
                        id: null,
                        naziv: null,
                        opis: null
                    }
                }).catch(error =>{
                    console.log(error);
                });
        },
        clearSelected_item(){
            this.selected_item= {
                id: null,
                naziv: null,
                opis: null,
            }
        },
        deleteKategorijaRobe(){
            api.delete("/api/kategorija_robe/"+ this.selected_item.id)
                .then(res =>{
                    this.getAllKategorijaRobe();
                    this.clearSelected_item();
                    this.deleteshow = false;
                }).catch(error =>{
                    console.log(error);
                })
        }
    },
    template: `
    <div style="min-height: 410px">
        <div class="container">
            <h2 class="text-center mt-4 mb-4">Tabela kategorija-robe</h2>

            <div class="text-center mb-4">
                <button class="btn btn-success" @click="clearSelected_item(); addshow = true">Add</button>
            </div>

            <table class="table mb-4 table-hover table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Naziv</th>
                        <th>Opis</th>
                        <th>Akcije</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="kategorija_robe in kategorija_robe" :key="kategorija_robe.id">
                        <td>{{kategorija_robe.id}}</td>
                        <td>{{kategorija_robe.naziv}}</td>
                        <td>{{kategorija_robe.opis}}</td>
                        <td>
                        <button class="btn btn-warning btn-sm" @click="selected_item={...kategorija_robe}; editshow= true">Edit</button>
                        <button class="btn btn-danger btn-sm" @click="selected_item={...kategorija_robe}; deleteshow= true">Delete</button>
                        
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': addshow}">
            <div class="modal-dialog" role="ducument">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Add kategorija robe</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="addshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="createKategorijaRobe">
                            <div class="form-group">
                                <label for="korisnik" class="form-label">Naziv:</label>
                                <input type="text" class="form-control" id="naziv" v-model="selected_item.naziv" placeholder="Naziv" required>
                            </div>
                            <div class="form-group">
                                <label for="prezime" class="form-label">Opis:</label>
                                <input type="text" class="form-control" id="opis" v-model="selected_item.opis" placeholder="Opis" required>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="addshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>



        <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': editshow}">
            <div class="modal-dialog" role="ducument">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Edit kategorija robe</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="editshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="updateKategorijaRobe">
                            <div class="form-group">
                                <label for="korisnik" class="form-label">Naziv:</label>
                                <input type="text" class="form-control" id="naziv" v-model="selected_item.naziv" placeholder="Naziv" required>
                            </div>
                            <div class="form-group">
                                <label for="prezime" class="form-label">Opis:</label>
                                <input type="text" class="form-control" id="opis" v-model="selected_item.opis" placeholder="Opis" required>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="editshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" :class="{'d-block': deleteshow}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Delete Kategorija robe</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="deleteshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2>Are you sure you want to delete this?</h2>
                        <h4 class="text-danger">Deletion can cause an additional deletions</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" @click="deleteKategorijaRobe">YES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" @click="deleteshow=false;">NO</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}