import api from "../api.js";
export default {
    data() {
        return {
            robe: [],
            korpa: [],
            kategorije_robe: [],
            selected_item: {
                id: null,
                naziv: null,
                slika: null,
                opis: null,
                cena: null,
                stanje: null,
                kolicina: null,
                kategorija_id: null
            }
        }
    },
    mounted(){
        this.getAllRobe();
    },
    methods:{ 
        getAllRobe(){
            api.get("/api/roba").then(res => {
                this.robe = res.data;
            }).catch(error => {
                console.log(error);
            });
        },
        clearSelected_item(){
            this.selected_item= {
                id: null,
                naziv: null,
                slika: null,
                opis: null,
                cena: null,
                stanje: null,
                kolicina: null,
                kategorija_id: null
            }
        },
        dodavanje(roba){
            console.log(roba);
            console.log(this.$refs[roba.id][0].value);
            if (this.$refs[roba.id][0].value == ""){
                this.showErrorNotification();
            }
            else{
                if(localStorage.getItem("korpa") != null){
                    this.korpa = JSON.parse(localStorage.getItem("korpa"));
                }
                this.korpa.push({...roba, kolicina: parseInt(this.$refs[roba.id][0].value)});
                this.$refs[roba.id][0].value= ""
                localStorage.setItem("korpa", JSON.stringify(this.korpa));
                this.showSuccessNotification();

            }
        },

        showSuccessNotification(){
            var toastElList = document.querySelectorAll('#toast1')[0];
            toastElList.classList.add("show");
            setTimeout(() => {
                toastElList.classList.remove("show");
            }, 1500);
        },
        showErrorNotification(){
            var toastElList = document.querySelectorAll('#toast2')[0];
            toastElList.classList.add("show");
            setTimeout(() => {
                toastElList.classList.remove("show");
            }, 1500);
        }

        

    },

    template: `
        <div style="min-height: 410px">
        <div class="toast fade hide bg-success text-white p-4 border rounded" id="toast1" style="position: fixed; top: 19%; left: 50%; transform: translate(-50%, -50%); width: 400px; z-index: 1;">
            <div class="toast-header text-white text-center mb-3 border-bottom">
                <span><i class="fa-solid fa-circle-check fa-lg icon-success"></i></span>
                <strong class="me-auto"><i class="bi-gift-fill"></i> Korpa info</strong>
            </div>
            <div class="toast-body text-center">
                <strong class="me-auto">Uspesno ste dodali novi proizvod u korpu</strong>
            </div>
        </div>
        <div class="toast fade hide bg-danger text-white p-4 border rounded" id="toast2" style="position: fixed; top: 19%; left: 50%; transform: translate(-50%, -50%); width: 400px; z-index: 1;">
            <div class="toast-header text-white text-center mb-3 border-bottom">
                <span><i class="fa-solid fa-circle-xmark fa-lg icon-success"></i></span>
                <strong class="me-auto"><i class="bi-gift-fill"></i> Korpa info</strong>
            </div>
            <div class="toast-body text-center">
                <strong class="me-auto">Potrebno je da izaberete kolicinu pre dodavanja</strong>
            </div>
        </div>
            <div class="container mt-3 d-flex p-4 flex-wrap justify-content-around">
                <div class="card" style="width:300px;margin-top:25px" v-for="roba in robe" :key="roba.id">
                    <img class="card-img-top" v-bind:src="'/assets/'+roba.slika" alt="Za datu robu nije dodata slika"  style="width: 100%; height: 50%">
                    <div class="card-body">
                        <h4 class="card-title">{{roba.naziv}}</h4>
                        <h5 class="card-subtitle mb-2 text-muted">Cena: {{roba.cena}} rsd</h5>
                        <p class="card-text">Opis: {{roba.opis}}</p>
                        <p class="card-text">Kolicina: <input type="number" :name="roba.naziv" :ref="roba.id" id="kolicina" style="width:50px" min="1" max="10" :disabled="roba.stanje == 'nedostupno'" required> </p>
                        <p class="card-text">Stanje: {{roba.stanje}}</p>
                        <button class="btn btn-warning btn-sm" @click="dodavanje(roba)" :disabled="roba.stanje == 'nedostupno'">Dodaj u korpu</button>
                    </div>
                </div>

                
            </div>
        </div>
    `
}