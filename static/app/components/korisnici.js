import api from "../api.js";
export default {
    data(){
        return {
            korisnici: [],
            addshow: false, 
            editshow: false,
            deleteshow: false,
            selected_item: {
                id: null,
                ime: null,
                prezime: null,
                korisnicko_ime: null,
                lozinka: null
            }

        }
    },
    mounted(){
        this.getAllKorisnici()
    },
    methods: {
        getAllKorisnici(){
            api.get("/api/korisnik").then(res => {
                this.korisnici = res.data;
            }).catch(error => {
                console.log(error);
            });
        },
        createKorisnik(){
            api.post("/api/korisnik",{
                ime: this.selected_item.ime,
                prezime: this.selected_item.prezime,
                korisnicko_ime: this.selected_item.korisnicko_ime,
                lozinka: this.selected_item.lozinka
            }).then(res => {
                this.getAllKorisnici();
                this.addshow = false;
                this.selected_item = {
                    id: null,
                    ime: null,
                    prezime: null,
                    korisnicko_ime: null,
                    lozinka: null
                };
            }).catch(error => {
                console.log(error);
            });
        },
        updateKorisnik(){
            console.log(this.selected_item);
            api.put("/api/korisnik/"+ this.selected_item.id, this.selected_item)
                .then(res =>{
                    this.getAllKorisnici();
                    this.editshow = false;
                    this.selected_item = {
                        id: null,
                        ime: null,
                        prezime: null,
                        korisnicko_ime: null,
                        lozinka: null
                    };
                }).catch(error =>{
                    console.log(error);
                });
        },
        clearSelected_item(){
            this.selected_item= {
                id: null,
                ime: null,
                prezime: null,
                korisnicko_ime: null,
                lozinka: null,
            }
        },
        deleteKorisnik(){
            api.delete("/api/korisnik/"+ this.selected_item.id)
                .then(res => {
                    this.getAllKorisnici();
                    this.clearSelected_item();
                    this.deleteshow = false;
                }).catch(error =>{
                    console.log(error);
                })
        }
    },
    template: `
    <div>
        <div class="container">
            <h2 class="text-center mt-4 mb-4">Tabela korisnici</h2>
            
            <div class="text-center mb-4">
                <button class="btn btn-success" @click="clearSelected_item(); addshow = true">Add</button>
            </div>

            <table class="table mb-4 table-hover table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Ime</th>
                        <th>prezime</th>
                        <th>Korisnicko Ime</th>
                        <th>Lozinka</th>
                        <th>Akcije</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="korisnik in korisnici" :key="korisnik.id">
                        <td>{{korisnik.id}}</td>
                        <td>{{korisnik.ime}}</td>
                        <td>{{korisnik.prezime}}</td>
                        <td>{{korisnik.korisnicko_ime}}</td>
                        <td>{{korisnik.lozinka}}</td>
                        <td>
                        <button class="btn btn-warning btn-sm" @click="selected_item={...korisnik}; editshow= true">Edit</button>
                        <button class="btn btn-danger btn-sm" @click="selected_item={...korisnik}; deleteshow= true">Delete</button>
                        
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': addshow}">
            <div class="modal-dialog" role="ducument">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Add korisnik</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="addshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="createKorisnik">
                            <div class="form-group">
                                <label for="korisnik" class="form-label">Ime</label>
                                <input type="text" class="form-control" id="korisnik" v-model="selected_item.ime" placeholder="Ime" required>
                            </div>
                            <div class="form-group">
                                <label for="prezime" class="form-label">Prezime</label>
                                <input type="text" class="form-control" id="prezime" v-model="selected_item.prezime" placeholder="Prezime" required>
                            </div>
                            <div class="form-group">
                                <label for="korisnicko_ime" class="form-label">Korisnicko ime</label>
                                <input type="text" class="form-control" id="korisnicko_ime" v-model="selected_item.korisnicko_ime" placeholder="Korisnicko ime" required>
                            </div>
                            <div class="form-group">
                                <label for="lozinka" class="form-label">Lozinka</label>
                                <input type="password" class="form-control" id="lozinka" v-model="selected_item.lozinka" placeholder="Lozinka" required>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="addshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>


        <div class="modal" role="dialog" tabindex="-1" :class="{'d-block': editshow}">
            <div class="modal-dialog" role="ducument">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Edit korisnik</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="editshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="updateKorisnik">
                            <div class="form-group">
                                <label for="korisnik" class="form-label">Ime</label>
                                <input type="text" class="form-control" id="korisnik" v-model="selected_item.ime" placeholder="Ime" required>
                            </div>
                            <div class="form-group">
                                <label for="prezime" class="form-label">Prezime</label>
                                <input type="text" class="form-control" id="prezime" v-model="selected_item.prezime" placeholder="Prezime" required>
                            </div>
                            <div class="form-group">
                                <label for="korisnicko_ime" class="form-label">Korisnicko ime</label>
                                <input type="text" class="form-control" id="korisnicko_ime" v-model="selected_item.korisnicko_ime" placeholder="Korisnicko ime" required>
                            </div>
                            <div class="form-group">
                                <label for="lozinka" class="form-label">Lozinka</label>
                                <input type="password" class="form-control" id="lozinka" v-model="selected_item.lozinka" placeholder="Lozinka" required>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" @click="editshow = false" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </form>
                    </div>
    
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" :class="{'d-block': deleteshow}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Delete Korisnik</h3>
                        <button class="close" data-dismiss="modal" aria-label="Close" @click="deleteshow = false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2>Are you sure you want to delete this?</h2>
                        <h4 class="text-danger">Deletion can cause an additional deletions</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" @click="deleteKorisnik()">YES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" @click="deleteshow=false;">NO</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}