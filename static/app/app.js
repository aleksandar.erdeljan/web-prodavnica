import Home from "./components/home.js";
import Register from "./components/register.js";
import Login from "./components/login.js";
import Logout from "./components/logout.js";
import Korisnici from "./components/korisnici.js";
import Roba from "./components/roba.js";
import Racun from "./components/racun.js";
import Kategorija_robe from "./components/kategorija_robe.js";
import Pravo_pristupa from "./components/pravo_pristupa.js";
import Dodeljeno_pravo from "./components/dodeljeno_pravo.js";
import Ponuda_robe from "./components/ponuda_robe.js";
import Korpa from "./components/korpa.js";
import Moji_racuni from "./components/moji_racuni.js";
//const Korisnici = {template: "<div>korisnici</div>"}
//const Kategorija_robe = {template: "<div>Kategorija_robe</div>"}
const routes = [
    {path: "/" , component: Home},
    {path: "/register", component: Register, meta: {requiresAuth: false}},
    {path: "/login", component: Login, meta: {requiresAuth: false}},
    {path: "/logout", component: Logout, meta: {requiresAuth: true}},
    {path: "/korisnici", component: Korisnici, meta: {requiresAuth: true, role: "admin"}},
    {path:"/roba", component: Roba, meta: {requiresAuth: true, role: "admin"}},
    {path:"/kategorija_robe", component: Kategorija_robe, meta: {requiresAuth: true, role: "admin"}},
    {path:"/racun", component: Racun, meta: {requiresAuth: true, role: "admin"}},
    {path:"/pravo_pristupa", component: Pravo_pristupa, meta: {requiresAuth: true, role: "admin"}},
    {path:"/dodeljeno_pravo", component: Dodeljeno_pravo, meta: {requiresAuth: true, role: "admin"}},
    {path:"/ponuda_robe",component: Ponuda_robe, meta: {requiresAuth: true, role: "kupac"}},
    {path:"/korpa", component: Korpa, meta: {requiresAuth: true, role: "kupac"}},
    {path:"/moji_racuni", component: Moji_racuni, meta: {requiresAuth: true, role: "kupac"}}
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next)=> {
    const prava_pristupa=JSON.parse(localStorage.getItem("prava_pristupa"));
    const requiresAuth= to.meta.requiresAuth;
    const role= to.meta.role;
    if (requiresAuth){
        if (role == null){
            next();
        }
        else if (prava_pristupa.includes(role)){
            next();    
        }
        else{
            next("/");
        }
    }
    else{
        next();
    }
    console.log(prava_pristupa);
});

const app = new Vue({
    router
}).$mount("#app");