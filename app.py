from flask import Flask;
from utils.db import mysql;
from blueprints.korisnik_bp import korisnik_bp
from blueprints.kategorija_robe_bp import kategorija_robe_bp
from blueprints.roba_bp import roba_bp
from blueprints.racun_bp import racun_bp
from blueprints.stavka_racuna_bp import stavka_racuna_bp
from blueprints.pravo_pristupa_bp import pravo_pristupa_bp
from blueprints.dodeljena_prava_bp import dodeljena_prava_bp
from blueprints.auth_bp import auth_bp
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token
from datetime import timedelta

app = Flask(__name__, static_url_path="/")

app.config["MYSQL_DATABASE_USER"]="root"
app.config["MYSQL_DATABASE_PASSWORD"]= "singidunum123"
app.config["MYSQL_DATABASE_DB"] = "prodavnica"
app.config["JWT_SECRET_KEY"] = "super-secret"
app.config["JWT_ACCESS_TOKEN_EXPIRES"]= timedelta(minutes=30)
jwt = JWTManager(app)

app.register_blueprint(korisnik_bp, url_prefix= "/api/korisnik")
app.register_blueprint(kategorija_robe_bp, url_prefix= "/api/kategorija_robe")
app.register_blueprint(roba_bp, url_prefix= "/api/roba")
app.register_blueprint(racun_bp, url_prefix= "/api/racun")
app.register_blueprint(stavka_racuna_bp, url_prefix= "/api/stavka_racuna")
app.register_blueprint(pravo_pristupa_bp, url_prefix= "/api/pravo_pristupa")
app.register_blueprint(dodeljena_prava_bp, url_prefix= "/api/dodeljena_prava")
app.register_blueprint(auth_bp, url_prefix= "/api/auth")

mysql.init_app(app)

@app.route("/")
def home():
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run(debug=True)

